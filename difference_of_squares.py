def square_of_sum(sq_s):
	s=0
	for i in range(1,sq_s+1):
		s+=i
	return s*s

def sum_of_squares(s_sq):
	s2=0
	for i in range(1,s_sq+1):
		s2+=i*i
	return s2


def difference(diff):
	sd1=square_of_sum(diff)
	sd2=sum_of_squares(diff)
	difference=sd1-sd2
	return difference
