from string import maketrans
def encode(string):
	ct=''
	string=string.lower()
	string=string.replace(',','') 
	string=string.replace('.','')
    	pt="abcdefghijklmnopqrstuvwxyz"
    	ct="zyxwvutsrqponmlkjihgfedcba"
    	trans=maketrans(pt,ct)
	ct = string.translate(trans)
	ct=ct.replace(' ','')	
	return ' '.join([ct[i:i+5] for i in range(0,len(ct),5)])			
    


def decode(string):
	string=string.replace(' ','')
    	pt="abcdefghijklmnopqrstuvwxyz"
    	ct="zyxwvutsrqponmlkjihgfedcba"
    	trans=maketrans(ct,pt)
    	#for i in string:
	return string.translate(trans)
