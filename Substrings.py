def count_substring(string, sub_string):
    count=0
    sublen=len(sub_string)
    for i in range(len(string)):
        if(string[i]==sub_string[0]):
                if(string[i:(i+sublen)]==sub_string):
                    count+=1
    return count
