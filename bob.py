def hey(string):
	string=string.strip()
	if string.isupper():
		return  "Whoa, chill out!"
	elif string == '':
		return "Fine. Be that way!"
	elif string.endswith('?'):
		return "Sure."
	else:
		return "Whatever."
