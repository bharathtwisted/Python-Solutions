def score(string):
	string=string.upper()
	count=0
	lis1=['A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T']
	lis3=['B','C','M','P']
	lis4=['F','H','V','W','Y'] 
	for i in string:
		if i in lis1:
			count+=1
		elif i=='D' or i=='G':
			count+=2
		elif i in lis3:
			count+=3
		elif i in lis4:
			count+=4
		elif i=='J' or i=='X':
			count+=8
		elif i=='Q' or i=='Z':
			count+=10
		else:
			count+=5
	return count
