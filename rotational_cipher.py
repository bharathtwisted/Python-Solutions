def rotate(string,key):
	ctf=""
	for i in string:
		if i.isalpha():
			ct=ord(i)+key
			if i.isupper():
				if (ct>ord('Z')):
					ct=ct-26
				ctf+=chr(ct)
			if  i.islower():
				ct=ord(i)+key
				if (ct>ord('z')):
					ct=ct-26
				ctf+=chr(ct)
		else:
			ctf+=i
	return ctf
