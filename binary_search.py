def binary_search(array, number):
	first=0
	last=len(array)-1
	while(first<=last):
		mid=(first+ last)//2
		if(array[mid]==number):
			return mid
		elif(array[mid]>number):
			last=mid-1
		elif(array[mid]<number):
			first=mid+1
	raise ValueError
		

