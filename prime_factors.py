def prime_factors(number):
	#global prime
	prime=[]
	while(number%2==0 and number>1):
		number=number//2
		prime+=[2]
	if(number==1):
		return prime
	for i in range(3,int(number**(0.5)+1),2):	
		while(number%i==0 and number>1 ):
			number=number//i
			prime+=[i]
	if (number!=1):
		prime+=[number]
	return prime
 

