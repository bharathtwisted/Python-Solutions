from string import maketrans
def to_rna(string):
    dna="GCTA"
    rna="CGAU"
    trans=maketrans(dna,rna)
    dna_list=['G','C','T','A']
    for i in string:
	if i not in dna_list:
		return ''
    for i in string:
	return string.translate(trans)
    
